## Audio Embed Field

Audio Embed Field creates a simple field type that allows you to embed audio.

Soundcloud and custom urls plugins are provided with module.

Submodules also provided for integration with Drupal core media and
media_entity.

Audio embed field simply creates an embedded audio player, to play attached
media.

### Requirements

For the _audio_embed_media_ module, you will need the _media_entity_ module.

For the _audio_embed_media_core_ module, you will need the core _media_ module.

The base module also requires _field_ and _image_ from core.

The module can make use of _colorbox_, but it is not required.

### Composer Install


### Install

* Install as usual as per http://drupal.org/node/895232 for further information.

* You can add Audio embed fields via the normal interface on content types.

* If using media_entity, the audio_embed_media module included provides
  a media_entity bundle bridge.

* If using core media, the audio_embed_media_core module provides a media source
for creating a new media type.

* When you add fields, select the providers you want to use,including Custom URL
  and Soundcloud. If using Soundcloud, you need to obtain a client ID
  from the Soundcloud Developers site: https://developers.soundcloud.com/


### Customize

* You may override the template files in the /templates folder in your theme.
  See Twig debugging output for possible override suggestions.


### Maintainers

Current maintainers:
* George Anderson (geoanders) - https://www.drupal.org/u/geoanders

Past maintainers:
* David Lohmeyer (vilepickle) - https://www.drupal.org/u/vilepickle
